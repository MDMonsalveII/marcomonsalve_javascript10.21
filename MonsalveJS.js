function ejercicio_uno() {
  var edad = prompt("Ingresa tu edad: ");
  if (edad < 18) {
    confirm("No puede conducir, es menor de edad.");
   } else {
    confirm("Puede conducir, es mayor de edad.");
   }
 }
function ejercicio_dos() {
    var nota = prompt("Ingresa tu nota: ");
    if (nota == 0 || nota <= 3) {
      confirm("Muy deficiente.");
    } else {
      if (nota < 3 || nota <= 5) {
        confirm("Insuficiente.");
      } else {
        if (nota < 5 || nota <= 6) {
          confirm("Suficiente.");
        } else {
          if (nota < 6 || nota <= 7) {
            confirm("Bien.");
          } else {
            if (nota < 7 || nota <= 9) {
              confirm("Notable.");
            } else {
              if (nota < 9 || nota <= 10) {
                confirm("Sobresaliente.");
              }
              else {
                confirm("Las notas deben estar entre 0 - 10.");
              }
            }
          }
        }
      }
    }
  }
function ejercicio_tres() {
    var radio = document.getElementsByName("radio_uno");
    for (var i = 0; i < radio.length; i++) {
      if (radio[i].checked == true) {
        confirm("El radio seleccionado es: " + (i + 1));
      }
    }
  }
function ejercicio_cuatro() {
    var valor = document.getElementById("texto_cuatro").value;
    var text = document.createTextNode(" " + valor + ".");
    document.getElementById('texto_div').appendChild(text);
  }
function ejercicio_cinco() {
    var a = parseInt(document.getElementById("a").value);
    var b = parseInt(document.getElementById("b").value);
    if (a > b) {
      alert("El número mayor es A: " + a);
    } else {
      if (b > a) {
        alert("El número mayor es B: " + b);
      } else {
        if (a == b) {
          alert("Los números A y B son iguales.");
        }
      }
    }
  }
function ejercicio_seis() {
    function aleatorio(min, max) {
      return Math.floor(Math.random() * (max - min)) + min;
    };

    var select_seis = document.getElementById("select_seis");
    var opciones = "";
    for (var i = 0; i < 10; i++) {
      opciones += "<option>" + aleatorio(1, 100) + "</option>";
        }
    select_seis.innerHTML = opciones;
    }
function ejercicio_siete() {
    var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    var t = meses.length;
    for (var i = 0; i < t; i++) {
      console.log(meses[i]);
        }
    }
function ejercicio_ocho() {
    var resultado = "";
    do {
      var cadena = prompt("Introduce una cadena");
      if (resultado == "") {
        resultado = resultado + cadena;
      } else {
        resultado = resultado + "-" + cadena;
      }
    } while (confirm("¿Desea escribir otra?"));
    document.getElementById('texto_div_ocho').innerHTML = resultado;
    }
function ejercicio_nueve() {
    var suma = 0;
    do {
      var numero = prompt("Introduce un numero");
      if (Number(numero) == numero) {
        numero = Number(numero);
        suma = suma + numero;
      } else {
        if (numero != undefined) {
          alert(numero + " No es un numero");
        }
      }
    } while (numero != undefined);
    document.getElementById('texto_div_nueve').innerHTML = suma;
    }
function ejercicio_diez() {
    var total = 0;
    var precio = prompt("Precio: ");
    precio = Number(precio);
    var iva = prompt("IVA: ");
    iva = Number(iva);
    calculo = Number(((precio * iva) / 100));
    total = Number(precio + calculo);
    document.getElementById('texto_div_diez').innerHTML = "Precio: " + precio + " IVA: " + iva + "<br>El Precio total es: " + total;
    }
function ejercicio_once() {
    var total = 0;
    var cadena = prompt("Ingrese un texto: ");
    var total = cadena.match(/[aeiou]/gi).length;
    document.getElementById('texto_div_once').innerHTML = "El total de vocales en el texto es: " + total;
    }
function ejercicio_doce() {
    const Caracter = (cadena, caracter, pasos) => {
      let cadenaCarac = "";
      var longitud = cadena.length;
      for (let i = 0; i < longitud; i += pasos) {
        if (i + pasos < longitud) {
          cadenaCarac += cadena.substring(i, i + pasos) + caracter;
        } else {
          cadenaCarac += cadena.substring(i, longitud);
        }
      }
      return cadenaCarac;
    }
    var cadena = prompt("Ingrese una cadena de texto: ");
    var texto = Caracter(cadena, "-", 1);
    alert("Cadena con guion (-): " + texto);
    }
function ejercicio_trece() {
     var nombre1 = prompt("1er Nombre:");
     var edad1 = Number(prompt("Edad:"));
  
     var nombre2 = prompt("2do Nombre:");
     var edad2 = Number(prompt("Edad:"));

     var nombre3 = prompt("3er Nombre:");
     var edad3 = Number(prompt("Edad:"));
     var maximo = Math.max(edad1, edad2, edad3);
  
     if (maximo == edad1) {
      alert("El mayor es: " + nombre1);
     }
     if (maximo == edad2) {
       alert("El mayor es: " + nombre2);
     }
     if (maximo == edad3) {
       alert("El mayor es: " + nombre3);
   }
 }   
 